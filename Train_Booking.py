class User:
    def _init_(self, username, password):
        self.username = username
        self.password = password

class Passenger(User):
    def _init_(self, name, username, password, age, gender):
        super()._init_(username, password)
        self.name = self.validate_name(name)
        self.age = age
        self.gender = gender
        
    def display_details(self):
        print("Details Submitted")
        print(f"Name: {self.name}\nAge: {self.age}\nGender: {self.gender}\n")

class Train:
    trains = {
        "1. Rajdhani Express": {
            "origin": "New Delhi",
            "destination": "Mumbai",
            "classes": {"1": "AC First Class", "2": "AC 2 Tier", "3": "AC 3 Tier"},
            "date": "2024-03-10",
            "departure_time": "08:00",
            "arrival_time": "20:00"
        },
        "2. Shatabdi Express": {
            "origin": "Chennai",
            "destination": "Bangalore",
            "classes": {"1": "Executive Class", "2": "Chair Car"},
            "date": "2024-03-11",
            "departure_time": "09:00",
            "arrival_time": "12:00"
        },
        "3. Duronto Express": {
            "origin": "Kolkata",
            "destination": "Delhi",
            "classes": {"1": "AC First Class", "2": "AC 2 Tier", "3": "AC 3 Tier"},
            "date": "2024-03-12",
            "departure_time": "10:00",
            "arrival_time": "23:00"
        },
        "4. Gatimaan Express": {
            "origin": "Delhi",
            "destination": "Agra",
            "classes": {"1": "AC Chair Car", "2": "AC Executive Class"},
            "date": "2024-03-13",
            "departure_time": "07:00",
            "arrival_time": "09:30"
        },
        "5. Tejas Express": {
            "origin": "Mumbai",
            "destination": "Goa",
            "classes": {"1": "AC Chair Car", "2": "AC Executive Class"},
            "date": "2024-03-14",
            "departure_time": "08:30",
            "arrival_time": "16:00"
        },
        "6. Humsafar Express": {
            "origin": "Ahmedabad",
            "destination": "Chennai",
            "classes": {"1": "AC 3 Tier", "2": "Sleeper Class"},
            "date": "2024-03-15",
            "departure_time": "11:00",
            "arrival_time": "06:30"
        },
        "7. Garib Rath Express": {
            "origin": "Mumbai",
            "destination": "Kolkata",
            "classes": {"1": "AC 3 Tier", "2": "Sleeper Class"},
            "date": "2024-03-16",
            "departure_time": "15:00",
            "arrival_time": "12:00"
        },
        "8. Sampark Kranti Express": {
            "origin": "Delhi",
            "destination": "Chandigarh",
            "classes": {"1": "AC Chair Car", "2": "Sleeper Class"},
            "date": "2024-03-17",
            "departure_time": "18:00",
            "arrival_time": "21:30"
        },
        "9. Vivek Express": {
            "origin": "Dibrugarh",
            "destination": "Kanyakumari",
            "classes": {"1": "AC 2 Tier", "2": "AC 3 Tier", "3": "Sleeper Class"},
            "date": "2024-03-18",
            "departure_time": "14:30",
            "arrival_time": "10:30"
        },
        "10. Jan Shatabdi Express": {
            "origin": "Amritsar",
            "destination": "Haridwar",
            "classes": {"1": "AC Chair Car", "2": "Sleeper Class"},
            "date": "2024-03-19",
            "departure_time": "05:45",
            "arrival_time": "11:00"
        }
    }
    
    
    def _init_(self, train_name):
        self.train_name = train_name
        self.details = self.trains[train_name]
        
        
    def display_train_details(self):
        classes_available = ', '.join(self.details['classes'].values())
        print(f"Train Name: {self.train_name}, Destination: {self.details['destination']}, Origin: {self.details['origin']}, Classes Available: {classes_available}, Date of Travel: {self.details['date']}, Departure Time: {self.details['departure_time']}, Arrival Time: {self.details['arrival_time']}\n")


class Ticket:
    def _init_(self, train, passengers):
        self.train = train
        self.passengers = passengers
    
    def book_ticket(self):
        total_fare = 0
        for passenger in self.passengers:
            age = passenger.age
            if age <= 14:
                fare = 250
            elif 14 < age <= 28:
                fare = 300
            elif 28 < age <= 56:
                fare = 350
            else:
                fare = 200
            total_fare += fare

        print()
        print(f"Total Fare: Rs {total_fare}")
        print() 
        
        choice = input("Do you want to proceed with payment? (yes/no): ")
        print()
        if choice.lower() == "yes":
            print("Payment successful. Ticket booked!")
            print()
            for idx, passenger in enumerate(self.passengers, 1):
                print(f"Passenger {idx}: {passenger.name}, Age: {passenger.age}, Gender: {passenger.gender}")
                print()
            cancel_choice = input("Do you want to cancel the ticket and return the money? (yes/no): ")
            print()
            if cancel_choice.lower() == "yes":
                print(f"Ticket canceled. Rs {total_fare} refunded.")
                return
            else:
                print("Thank you for booking! Have a nice day.")
        else:
            print("Booking cancelled.")
            
def register(users):
    name = input("Enter your name: ")
    username = input("Enter username: ")
    password = input("Enter password: ")
    age = int(input("Enter your age: "))
    
    if age < 18:
        print("You must be 18 years or older to register.")
        return None

    gender = input("Enter your gender (male/female/other): ").lower()
    while gender not in ['male', 'female', 'other']:
        print("Invalid gender. Please enter 'male', 'female', or 'other'.")
        gender = input("Enter your gender (male/female/other): ").lower()

    for user in users:
        if user.username == username:
            print("Username already taken.")
            return None

    new_user = Passenger(name, username, password, age, gender)
    users.append(new_user)
    print("Registration successful. Please login.")
    return new_user

def login(users):
    login_attempts = 0  
    while login_attempts < 3:  
        username = input("Enter username: ")
        password = input("Enter password: ")
        for user in users:
            if user.username == username and user.password == password:
                print("Login successful.")
                return user
        print("Invalid username or password. Please try again.")
        login_attempts += 1  
    print("Maximum login attempts reached. Please register.")
    return None

users = []
while True:
    print("\n1. Login\n2. Register\n3. Exit")

    choice = input("Enter your choice: ")

    if choice == '1':
        user = login(users)
        if user:
            print("Login successful.")
            print()
            print("Here are some trains available for booking:")
            print()
            
            for train_name, details in Train.trains.items():
                print(f"Train Name: {train_name}")
                print(f"Destination: {details['destination']}")
                print(f"Origin: {details['origin']}")
                print(f"Classes Available: {', '.join(details['classes'].values())}")
                print(f"Date of Travel: {details['date']}")
                print(f"Departure Time: {details['departure_time']}")
                print(f"Arrival Time: {details['arrival_time']}\n")
                
            choice_train = input("Enter the number corresponding to the train you want to book: ")
            print()
            
            train_name = next((key for key in Train.trains.keys() if key.startswith(choice_train)), None)
            
            if train_name:
                train = Train(train_name)
                train.display_train_details()
                class_type = input("Enter class (1/2/3): ")
                print()
                num_passengers = int(input("Enter number of passengers: "))
                passengers = []
                for i in range(num_passengers):
                    
                    print(f"\nPassenger {i + 1}:")
                    name = input("Enter passenger name: ")
                    age = int(input("Enter passenger age: "))
                    gender = input("Enter passenger gender(male/female/other): ")
                    
                    passengers.append(Passenger(name, user.username, user.password, age, gender))
                    
                ticket = Ticket(train, passengers)
                ticket.book_ticket()
                break

            else:
                print("Invalid train number. Please try again.")

        else:
            print("Invalid username or password. Please try again.")
        
            
    elif choice == '2':
        new_user = register(users)
        if new_user:
            users.append(new_user)

    elif choice == '3':
        print("Exiting...")
        break
    
    else:
        print("Invalid choice. Please enter a valid option.")
